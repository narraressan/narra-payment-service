# narra-payment-service

a generic payment service to test out stripe API and rabbitMQ


### Setup local dev
``` bash
# install dependencies
npm install --verbose

# start the application
npm start:api
npm start:gateways

# to run tests, simply execute
# make sure docker is running?
npm run test

# to run the test UI for stripe
cd tests/ui
http-server --ssl --cert cert.pem --key key.pem

# run and fix linter manually. 
# this should also be triggered during pre-commit
npm run lint
npm run lint:fix

# run dev via docker-compose - highly recommended!
# check localhost:15672 for the rabbitmq dashboard
docker-compose --file ./docker/dev/dev.yml up -d --build

# view logs
docker-compose --file ./docker/dev/dev.yml logs -f --tail 10
```
 
Read more about rabbitMQ [here](https://www.rabbitmq.com/tutorials/tutorial-one-python.html) as it is used to queue the payment requests for this app.


### Build and Publish docker
Note: this is not production ready yet


### Future releases
- add support for other payment gateway like stripe, dragonpay, paypal, etc.
- add bank api support (e.g. union bank ph, etc.)


> more to come soon probably a full blown service derived from this simple project ...