const StripePay = require('./gateways/stripePay');
const { log } = require('./utils');
const { CHANNELS_STRIPE } = require('./config');


// external services sends payment reqests to the queue
// then this script process the records from that queue
(async () => {
  const gateway = await new StripePay(CHANNELS_STRIPE);
  await gateway.connect()

  gateway.subscribe();

  const health = await gateway.isHealthy();
  log.info(`payment-service::`, health)
})();