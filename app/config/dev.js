const base = require('./base');
const { CHANNELS } = require('../utils/constants')


module.exports = {
  ...base,

  // extended settings
  API_HOST: process.env.API_HOST,
  API_PORT: process.env.API_PORT,
  QUEUE_HOST: process.env.QUEUE_HOST,
  QUEUE_PORT: process.env.QUEUE_PORT,
  QUEUE_USER: process.env.QUEUE_USER,
  QUEUE_PASS: process.env.QUEUE_PASS,

  // list of available channels
  CHANNELS_STRIPE: process.env.STRIPE_CHANNEL_NAME || CHANNELS.STRIPE,
};
