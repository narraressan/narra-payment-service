const dev = require('./dev');
const prod = require('./prod');


let config = dev;
if (process.env.ENV === 'PRODUCTION') {
  config = prod;
}


module.exports = config;
