const http = require('http');
const Koa = require('koa');
const koaBody = require('koa-body');
const cors = require('@koa/cors');
const { log } = require('./utils')


// internal dependencies
const healthAPI = require('./apis/health');


// default config
const { HOST, PORT } = require('./config');


const app = new Koa();


// initialize middlewares
app.use(koaBody());
app.use(cors());
app
  .use(healthAPI.routes())
  .use(healthAPI.allowedMethods());


// start the server
const server = http.createServer(app.callback());
server.listen(PORT, HOST, () => { log.info(`payment-api::init@${HOST}:${PORT}`) })


module.exports = app