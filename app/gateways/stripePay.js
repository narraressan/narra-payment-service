const axios = require('axios');
const amqp = require('amqplib');
const stripe = require('stripe');
const {
  API_HOST,
  API_PORT,
  QUEUE_HOST,
  QUEUE_PORT,
  QUEUE_USER,
  QUEUE_PASS,
  STRIPE_TOKEN } = require('../config')
const { sleep, log } = require('../utils')
const { AUTH_HEADER } = require('../utils/constants')


class StripePay {
  // write the isComplete and charge information in the database
  constructor(channel){
    this.target = channel;
    this.amqpAddress = `amqp://${QUEUE_USER}:${QUEUE_PASS}@${QUEUE_HOST}:${QUEUE_PORT}`;
    this.queue = null;
    this.stripeAPI = stripe(STRIPE_TOKEN);
  }


  async connect() {
    let retries = 0
    while(retries <= 10) {
      try {
        log.info(`on-init-attempt: ${this.target} ${this.amqpAddress}`)

        const conn = await amqp.connect(this.amqpAddress)
        this.queue = await conn.createChannel();
        await this.queue.assertQueue(this.target);

        break;
      }
      catch(err) {
        retries++;
        log.error(err)
        await sleep(5000);
      }
      log.warn('Reconnection-attempts:', retries)
    }
  }


  async isHealthy(){
    log.info(`is-healthy-attempt: ${this.target}`)
    return await this.queue.assertQueue(this.target)
  }


  // subscribe and get transactions from rabbitMQ channel
  subscribe(){
    this.queue.consume(this.target, (message) => {
      const transaction = JSON.parse(message.content.toString())

      this.updateChargeStatus(transaction)

      // after consuming, call acknowledge to remove from queue
      this.queue.ack(message)
    })
  }


  // publish transactions to a rabbitMQ channel
  publish(transaction){
    try {
      const tmp = JSON.stringify(transaction)
      this.queue.sendToQueue(this.target, Buffer.from(tmp));
    }
    catch(err) {
      return false
    }

    return true
  }


  // https://stripe.com/docs/payments/accept-a-payment-charges#node
  pay(transaction) {
    log.info(`transaction::`, transaction)
    return this.stripeAPI.charges.create({
            amount: transaction.amount,
            currency: transaction.currency.toLowerCase(),
            description: transaction.invoice,
            source: transaction.token.id,
          })
  }


  // call api endpoint to store the stripe charge and flag successful and complete - keep async
  updateChargeStatus(transaction) {
    this.pay(transaction)
          .then((charge) => {
            this.recordChargeResult({ transaction, charge })
          })
          .catch((err) => {
            log.error('Stripe-failed::', err)
            this.recordChargeResult({ transaction, error: err })
          })
  }


  // update the database record
  recordChargeResult(fullTransaction) {
    axios({
      method: 'PUT',
      url: `http://${API_HOST}:${API_PORT}/checkout/stripe`,
      data: fullTransaction,
      headers: {
        [AUTH_HEADER]: fullTransaction.transaction.auth
      }
    })
    .then((res) => {
      log.info(JSON.stringify(res.data));
    })
    .catch(err => log.error('API-update-failed::', err));
  }
}


module.exports = StripePay;
