/**
 * @jest-environment node
 */

// const Docker = require('dockerode');
// const { GenericContainer } = require('testcontainers');
const request = require('supertest');
const nock = require('nock');
const app = require('../server');
const { AUTH_HEADER } = require('../utils/constants');
const {
  API_HOST,
  API_PORT,
  CHANNELS_STRIPE } = require('../config')
const StripePay = require('../gateways/stripePay')
const { sleep } = require('../utils')
const stripeToken = require('./stripeToken.js')


describe('Check health of api', () => {
  it('should check the health of the service', async (done) => {
    const res = await request(app.callback()).get('/health/stripe').send()

    expect(res.statusCode).toEqual(200)
    expect(res.body.queue).toEqual('test-stripe-queue')

    done();
  })
})


describe('Check health of api', () => {
  beforeEach(() => {
    nock(
      `http://${API_HOST}:${API_PORT}`,
      {
        reqheaders: {
          [AUTH_HEADER]: /[a-zA-Z0-9]/
        }
      })
      .persist()
      .put('/checkout/stripe')
      .reply(200, { message: 'complete' });
  });

  it('should check the health of the service', async (done) => {
    let stats = null;
    const gateway = await new StripePay(CHANNELS_STRIPE);
    await gateway.connect()

    const tests = 1
    for (var i = 0; i < tests; i++) {
      gateway.publish({

        // amount = 9.95 --> stripe accepts integer format but last 2 digits are decimal places
        amount: Math.round(9.95*100),

        currency: 'USD',
        invoice: `test-payment_uuid-${Math.random()}`,
        token: stripeToken,
        auth: `test-customer_jwt-${Math.random()}`,
        user: `test-customer_uuid-${Math.random()}`
      })
    }

    // wait for all messages to be ublished
    await sleep(3000);

    stats = await gateway.isHealthy();
    expect(stats.queue).toEqual('test-stripe-queue')
    expect(stats.messageCount).toBeGreaterThanOrEqual(tests)

    gateway.subscribe();

    // wait for all messages to be consumed
    await sleep(5000);

    stats = await gateway.isHealthy();
    expect(stats.messageCount).toEqual(0)

    done()
  })
})