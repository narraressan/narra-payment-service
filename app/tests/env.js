// in preparation for using testcontainers-node - requires WSL 2 or docker desktop not toolbox
process.env.DEBUG = 'testcontainers';
process.env.DOCKER_HOST = 'tcp://192.168.99.100:2376';

process.env.API_HOST = 'narra-stripe-api';
process.env.API_PORT = 3000;
process.env.QUEUE_HOST = 'localhost';
process.env.QUEUE_PORT = 5672;
process.env.QUEUE_USER = 'narra';
process.env.QUEUE_PASS = 'LionCity';

process.env.STRIPE_CHANNEL_NAME = 'test-stripe-queue';