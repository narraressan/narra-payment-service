const { createLogger, format, transports } = require('winston');


const { combine, timestamp, printf } = format;
const file = require('rotating-file-stream');
const path = require('path');


const log = createLogger({
  format: combine(
    timestamp(),
    format.printf(({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`),
  ),
  transports: [
    new transports.Console(),
    new transports.File({ filename: path.join(__dirname, `../logs/logs.log`) }),
  ],
});


const sleep = (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});


module.exports = { log, sleep }