const { log } = require('../utils')

class BaseException {
  constructor(code = 500, error = 'BASE_EXCEPTION', message = null, context) {
    log.error(`${error}: ${message}`)

    this.ctx = context;
    this.ctx.response.status = code;
    this.ctx.body = { error, message };
  }
}


class ServerException extends BaseException {
  constructor(context, message) {
    super(500, 'SERVER_FAILED', message, context);
  }
}


class StripeServiceFailedException extends BaseException {
  constructor(context, message) {
    super(500, 'NO_STRIPE_OR_DB', message, context);
  }
}


module.exports = {
  ServerException,
  StripeServiceFailedException
};
