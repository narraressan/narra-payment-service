const Router = require('koa-router');

const api = new Router({ prefix: '/health' });
const {
  ServerException,
  StripeServiceFailedException
} = require('../utils/exceptions');
const StripePay = require('../gateways/stripePay');
const { CHANNELS_STRIPE } = require('../config')


/**
 * [POST] `/pay`
 * - execute the actual payment function
 *
 * @auth NA
 * @query NA
 * @params NA
 * @body NA
 * @success {
 *    {string} error,
 *    {string} message
 * }
 * @error {
 *    {string} queueName,
 *    {string} subscribers,
 *    {string} tasksCount
 * }
 */
api.get('/stripe', async (ctx) => {
  try {
    const gateway = await new StripePay(CHANNELS_STRIPE);
    await gateway.connect()
    const stats = await gateway.isHealthy();

    if (stats != null) {
      ctx.response.status = 200;
      ctx.body = stats;
    } else {
      new StripeServiceFailedException(ctx, 'Stripe queue is not connected properly');
    }
  } catch (err) {
    new ServerException(ctx, err);
  }
});


module.exports = api;